= Integrating with SaaS applications
Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


Use case: Providing integration with SaaS applications, platforms, and services empowers organizations to build and run scalable
applications in modern, dynamic environments such as public, private, and hybrid clouds. 

Open the  diagrams in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your
content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/integrate-saas-applications.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/integrate-saas-applications.drawio?inline=false[[Download Diagram]]
--

--
image:logical-diagrams/integrating-with-saas-applications-ld.png[350,300]
image:logical-diagrams/integrating-with-saas-applications-details-ld.png[350,300]
image:schematic-diagrams/saas-external-crm-integration-sd.png[350,300]
image:schematic-diagrams/saas-external-crm-connector-sd.png[350,300]
image:schematic-diagrams/saas-integration-3rd-party-platform-sd.png[350,300]
image:schematic-diagrams/saas-integration-3rd-party-process-sd.png[350,300]
image:detail-diagrams/external-saas-crm.png[250,200]
image:detail-diagrams/crm-connector.png[250,200]
image:detail-diagrams/web-app.png[250,200]
image:detail-diagrams/api-management.png[250,200]
image:detail-diagrams/front-end-microservices.png[250,200]
image:detail-diagrams/process-facade-microservices.png[250,200]
image:detail-diagrams/integration-microservices.png[250,200]
image:detail-diagrams/integration-data-microservices.png[250,200]
image:detail-diagrams/sso-server.png[250,200]
image:detail-diagrams/3rd-party-platform-services.png[250,200]
--

